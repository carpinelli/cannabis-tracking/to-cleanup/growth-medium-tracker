#!/usr/bin/env python3

"""Parse, split, join, and build reports."""

import pandas as pd


COLUMNS = ['Room', 'Strain', 'Amount', 'Rockwool Date',
           'Volume (in.3)', 'Plant Height (in.)', 'Grow Medium',
           'Bloom Date', 'Days in Veg']


def extract_bloom_placed(filename: str) -> pd.DataFrame:
    """"""
    df = pd.read_excel(filename, header=2)
    column_check = [COLUMNS[0] in df, COLUMNS[1] in df,
                    COLUMNS[6] in df, COLUMNS[7] in df]
    if True not in column_check:
        return None  # No data in 'Bloom Placed'

    null = df.isnull().all(axis=1)  # Which rows are all null
    # Get the first index of the rows which are all null,
    # subtract 1 to get the last row needed for our table
    drop_from = (null.loc[null == True].index[0] - 1)

    bloom_placed = df.truncate(after=drop_from)
    # Drop all values not found in 'COLUMNS'
    bloom_placed = bloom_placed[COLUMNS]
    return bloom_placed


def merge_bloom_placements(files: list) -> int:
    """"""
    frames = list()

    for f in files:
        frames.append(extract_bloom_placed(f))

    master_frame = pd.concat(frames, ignore_index=True)
    master_frame.to_csv("BloomPlaced.csv", header=COLUMNS, index=False, encoding="utf-8")
    return 0
