from pathlib import Path
import re

import pymsgbox


def list_files(pattern: str,
               directory: (Path, str) = Path(".").resolve()) -> list:
    """"""
    pattern = re.compile(pattern)
    files = list()

    for file in directory.iterdir():
        pattern_match = pattern.search(file.stem)
        if pattern_match is not None:
            files.append(file)
    # Exit program if no CSV files are found
    if len(files) == 0:
        msg = ("No files matching pattern found in:\n\n"
               f"{directory}")
        pymsgbox.alert(msg)
        raise RuntimeError(msg)  # No files found
    return files
