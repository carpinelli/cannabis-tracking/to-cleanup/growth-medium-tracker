#!/bin/bash

# Setup Git for a new project

git init
# git set remote origin git@gitlab.example.com:namespace/nonexistent-project.git
git add .
git commit -m "Initial commit."
git push --set-upstream \
	git@gitlab.example.com:namespace/nonexistent-project.git master
